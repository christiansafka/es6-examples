class TaskCollection {

	constructor(tasks = [])
	{
		this.tasks = tasks;
	}

	dump() 
	{
		//this.tasks.forEach(task => `1: $(task)`);
		this.tasks.forEach((task, value) => 
			console.log(`${value + 1}: ${task}`));
	}
}

export default TaskCollection;

 