import TaskCollection from './TaskCollection.js';

new TaskCollection([
	'task1', 
	'task2', 
	'task3'
]).dump();


class Calculator {

	static sum(...numbers)
	{
		return numbers.reduce((prev, current) => prev+current);
	}
}

nums = [5, 6, 2, 1, 300];

setTimeout(function()
{
	console.log(Calculator.sum(...nums));
}, 1000);

var thing = new Promise(function(resolve, reject) {
	//do stuff
	setTimeout(function() { resolve(); }, 2000);
});

thing.then(function() 
{ 
	console.log("promise is completed"); 
	throw "fail";
})
.catch(function()   //catches thing.then, not the promise function.
{
	console.log("promise failed");
});

//more ES6

/*
	string.includes()
		  .startsWith()
		  .endsWith()
		  .repeat() 

	
	object destructuring:

	person = object with a name and age value

	let {name, age} = person;
		-creates 2 variables name and age using person obj.

	always use let not var, change let to const for optimization.

	also object shorthand and method shorthand.

*/