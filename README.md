# ES6 Examples #


Requires installation of rollup and rollup-plugin-buble through npm.  (npm install will only install rollup-plugin-buble)

includes examples of:

* classes
* import, export
* arrow syntax
* let/const instead of var
* rest and spread operators
* template strings
* object destructuring
* promises
* new string methods

missing: object shorthand, method shorthand