var TaskCollection = function TaskCollection(tasks)
{
	if ( tasks === void 0 ) tasks = [];

	this.tasks = tasks;
};

TaskCollection.prototype.dump = function dump () 
{
	//this.tasks.forEach(task => `1: $(task)`);
	this.tasks.forEach(function (task, value) { return console.log(((value + 1) + ": " + task)); });
};

new TaskCollection([
	'task1', 
	'task2', 
	'task3'
]).dump();


var Calculator = function Calculator () {};

Calculator.sum = function sum ()
{
		var numbers = [], len = arguments.length;
		while ( len-- ) numbers[ len ] = arguments[ len ];

	return numbers.reduce(function (prev, current) { return prev+current; });
};

nums = [5, 6, 2, 1, 300];

setTimeout(function()
{
	console.log(Calculator.sum.apply(Calculator, nums));
}, 1000);

var thing = new Promise(function(resolve, reject) {
	//do stuff
	setTimeout(function() { resolve(); }, 2000);
});

thing.then(function() 
{ 
	console.log("promise is completed"); 
	throw "fail";
})
.catch(function()   //catches thing.then, not the promise function.
{
	console.log("promise failed");
});

//more ES6

/*
	string.includes()
		  .startsWith()
		  .endsWith()
		  .repeat() 

	
	object destructuring:

	person = object with a name and age value

	let {name, age} = person;
		-creates 2 variables name and age using person obj.

	always use let not var, change let to const for optimization.

	also object shorthand and method shorthand.

*/